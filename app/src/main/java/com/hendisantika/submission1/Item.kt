package com.hendisantika.submission1

/**
 * Created by hendisantika on 18/09/18  19.35.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
data class Item(val name: String?, val image: Int?, val details: String?)