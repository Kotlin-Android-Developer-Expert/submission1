package com.hendisantika.submission1

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.*

/**
 * Created by hendisantika on 13/10/18  21.32.
 * email -> hendisantika@gmail.com
 * telegram --> @hendisantika34
 */
class ItemDetailsActivity : AppCompatActivity() {
    var name: String = ""
    var groupIcon: Int = 0
    var groupDetails: String = ""

    lateinit var nameTextView: TextView
    lateinit var logoView: ImageView
    lateinit var detailsView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        verticalLayout {
            padding = dip(16)
            logoView = imageView().lparams(width = matchParent) {
                height = dip(100)
                width = dip(100)
                gravity = Gravity.CENTER
            }
            nameTextView = textView {
                textSize = 15F
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
            detailsView = textView().lparams(width = matchParent) {
                textAlignment = View.TEXT_ALIGNMENT_CENTER
            }
        }

        val intent = intent
        name = intent.getStringExtra("name")
        groupIcon = intent.getIntExtra("logo", 0)
        groupDetails = intent.getStringExtra("details")

        logoView.setImageResource(groupIcon)
        nameTextView.text = name
        detailsView.text = groupDetails

    }
}